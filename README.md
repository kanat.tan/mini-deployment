# Basic Deployment Tips & Tricks

## Why do we need to deploy?

Let's face it. The development environment was optimized for development&mdash;to make developers productive. This means many decisions weren't made in the interest of handling many users, handling "bad" users, etc. It's simply not ready for the mass. 

Some of the differences are worth highlighting here:

* Single-user (single-threaded) vs. multi-user readiness
* When something fails, showing off where things fail (for ease of troubleshooting & debugging) vs. being discreet about it (so we don't exposed too much of how our code works, inviting in more attacks, yay!)
* Bulky (so we have "debug" info) vs. lean (optimized for load speed and bandwidth usage).  Additionally, minified + uglified means difficult to read/reverse-engineer, which in turn, helps(?) with obfuscation and more secrecy(?).

**The state-of-the-art "best" practice**: Continuous integration/continuous delivery (CI/CD) &mdash; image when you push your latest feature update/changes, your code goes through a battery of automated tests and if passed, is "built" automatically and deployed automatically without a fuss.  Not in this minicourse, though :(.

## Modern Architecture: A Simple Setup

This basic set up involves just three (main) components, but it's pretty general and can be applied in a broad range of scenarios. The components are:

1. the backend container (might involve a DB or something on the side)
2. the frontend container (often, this is just vanilla `nginx` with volume mapped correctly)

and a **gateway** (aka. proxy) container that glues them together. For this lesson, we'll use `nginx` as a gateway/proxy.

### Why's the gateway/proxy container for?

To simplify matters (as well as saving ourselves from other technical complications), we would like the following kind of behavior: Say the entirety of our app will be located under `groggy.awesome.com` but

* Everything going under `/api/` will be routed to the backend container
* Everything else will be routed to the frontend container

## Digression: `ngrok`

Setting up a complete stack for a web app can be time consuming. `ngrok` is a product (with a free, limited use license) that lets you easily set up a publicly-accessible, secure (`https`) URL which tunnels directly to the application setup on your machine. It lets you monitor accesses for debugging, etc. This is best illustrated by a demo ([`01-ngrok-demo`](01-ngrok-demo)).

* We will need an account
* We will need an `ngrok` binary for your platform. Follow the instructions on the website.
* We will need to put in an authtoken via `ngrok auth <token>`

After that, we'll run it like so:

```bash
./start.sh # to boot up a docker container for a demo nginx
/path/to/ngrok http 80 # start an "http" tunnel to port 80 on the localhost
```

## Using `nginx` for Custom Routing

For starters, `nginx` is a high-performance web server, but it can double up as a (reverse) proxy server. Juggling connections is its strong point. It's great at acting as a middleperson between incoming requests and the right "server" to dispatch the request to. To demonstrate custom routing, consider the following set up:

* If you request `/favicon.ico`, say I don't have one
* If you request for anything `/meow/`, proxy it to `https://muic.mahidol.ac.th/eng/`. For example, if there is a request for  `/meow/abc`, then we'll request it from `https://muic.mahidol.ac.th/eng/abc`.
* If you request for anything `/woof/`, proxy it to `https://mahidol.ac.th/`.
* Everything else is served from the local filesystem `/usr/share/nginx/html`.

The demo for this portion is [`02-proxy`](02-proxy/).  The following configuration is the main workhorse that instructs `nginx` to route according to our specifications:

```nginx
server {
    listen 80;


    root /usr/share/nginx/html;
    index index.html index.htm;

    server_name localhost;

    location = /favicon.ico {
        return 404;
    }

    location /meow/ {
        proxy_pass https://muic.mahidol.ac.th/eng/;
    }


    location /woof/ {
        proxy_pass https://mahidol.ac.th/;
    }
}
```

## Component Preparation and Let the Wild Rumpus Start!

We are going to deploy a webapp that is similar to what you've seen from last time:

*  The frontend was basically from last time, except that it interacts with a backend via `/api`
* The backend does nothing spectacular. It simply returns a random number upon request in the JSON format.

Our goal is to piece them together and run it in production mode.

### Quick Application Walkthrough

The pieces are called `frontend` and `backend` located inside [`03-simple-app`](03-simple-app/). To get a feel for what it looks like, we'll start it up in development mode as follows:

* Inside `backend`, we'll run `FLASK_APP=app flask run`, of course, after activating the `pipenv` shell and calling `pipenv install`.
* Inside `frontend`, we'll run `yarn serve`, of course, after running `yarn install`.

Let's now navigate to the frontend URL and play with it a bit.

### Preparing Flask

The development server is lacking in several ways. It is single-threaded, not hardened, too revealing, etc.&mdash;all in the name of convenience and productivity of the developers. To use Flask in production, we'll switch to a more production-ready web server. There are several options. We'll use `gunicorn` for this demo. We will do this in two steps:

1. Install and try out `gunicorn` on the command line
2. Package this as a Docker image

To proceed:

```bash
pipenv install gunicorn # Install the latest gunicorn
# Listen on port 5000 (don't forget the colon) 
# Use 4 workers
# Boot up the application "app" inside app
gunicorn --bind :5000 --workers 4 app:app 
```

Further instructions about `gunicorn` can be found [here](https://gunicorn.org/).

##### Dockerization

The following `Dockerfile` is pretty standard. We'll start with a Python 3.9 image, dump everything in there, and ask pipenv to install the remaining packages. Finally, we set the entry point to be `gunicorn` as follows:

```dockerfile
FROM python:3.9

# Set work dir
WORKDIR /usr/src/app

# Upgrade pip and install pipenv
RUN pip install --upgrade pip
RUN pip install pipenv

# Create a list of packages to install and install them
COPY ./Pipfile* /tmp/
RUN cd /tmp && pipenv lock --requirements > /usr/src/app/requirements.txt
RUN pip install -r requirements.txt

# Copy all our source files into the app folder
COPY . /usr/src/app/

# Tell Docker we're expected to expose port 5000
EXPOSE 5000

# Set the entry point
ENTRYPOINT ["gunicorn", "--bind", ":5000", "--workers", "4", "app:app"]
```

Inside the `backend` folder, we can build it like so&mdash;and test it out quickly.

```bash
docker build -t mybackend .
docker run -t -p5000:5000 mybackend

## Don't forget to stop & remove the test run
```

This results in a Docker image called `mybackend`.

### Preparing VueJS 

Frontend frameworks such as VueJS have the property that we can compile it down to static files and serve them as such for deployment. This means all we need is to create the relevant files, package them up, and let them be served using a standard web server.

Using `yarn`, we build the project for production like so:

```bash
yarn install # Make sure all the packages/dependencies are installed/updated
yarn build
```

This results in a compact, nice-looking folder called `dist/`.  And really, this is the only folder than has to be presented to the browser in order for our frontend side of the app to work.

###### Dockerization

```dockerfile
FROM nginx:1.21

# Copy the packaged app into the www root
COPY dist/ /usr/share/nginx/html
```

Inside the `backend` folder, we can build it like so&mdash;and test it out quickly.

```bash
docker build -t myfrontend .
docker run -t -p8000:80 myfrontend

## Don't forget to stop & remove the test run
```

This results in a Docker image called `myfrontend`.

### The Proxy/Gateway Config

If we started both the frontend and backend containers, we would have two containers running in isolation. To complete the picture, we will put in a third piece: the gateway/proxy. We expect it to have the following properties:

* Anything for `/api` goes to the backend container.
* Everything else goes to the frontend container.

This can be arranged quite easily by modifying our previous demo code slightly. The code for this portion lives inside `03-simple-app/proxy`.

```nginx
server {
    listen 80;
    server_name localhost;

    location /api/ {
        proxy_pass http://mybackend:5000/;
    }

    location / {
        proxy_pass http://myfrontend:80/;
    }
}
```

### Let's Roll!

Booting up the frontend and backend containers are pretty standard. For convenience, we have written scripts to start them up, called `start-frontend.sh` and `start-backend.sh`. There is a bit of trickiness in starting up the proxy. The thing is, the proxy container needs to be aware of the frontend and backend containers. We can accomplish this by adding the `--link` flag, like so:

```bash
#!/bin/bash

SCRIPT_ROOT="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
VERSION="1.21"

docker run --name myproxy \
        --publish 80:80 \
        --restart=always \
        --link mybackend \   # <-- hey, be aware of mybackend
        --link myfrontend \  # <-- hey, be aware of myfrontend
        --detach \
        --volume ${SCRIPT_ROOT}/default.conf:/etc/nginx/conf.d/default.conf \
        nginx:${VERSION}

```

We can bring them up. If we aren't feeling too adventurous, start the frontend and backend containers first, followed by the proxy. That's it! We can now enjoy our awesome web app.

Before we move on, let's add `ngrok` to the mix, so the app can be accessed from outside:

```bash
/path/to/ngrok http 80
```

## Optional: Adding SSL Support via The Sidecar Pattern

What if we have our own domain already? `ngrok` is nice but we want a professional looking/sounding URL, e.g., `https://groggy.awesome.com`. 

Some Background & Our Cast:

* (Not discussed here) Point the DNS to our server's IP address means that if we access, say,  `groggy.awesome.com`, requests will be sent to our server.
* Our `nginx` setup so far will listen on port `80`, the default `http` port. 
* Increasingly people demand security! `https` is not only faster but also more secure. What not to like about it!
* To use `https`, we need a certificate&mdash;a piece of "paper" that says we really are who we claim to be. In this case, this server that you're really talking to is seriously honest-to-god `groggy.awesome.com` and this is not a lie!
* Meet our savior! [Let's Encrypt](https://letsencrypt.org/) is a nonprofit provider of free certificates.
* How Let's Encrypt works is beyond the scope of this lesson. Learn about it on the Internet.

**Our goal:** Use ~~Magic!~~ sidecar containers to automatically obtain and add `https` (TLS)  to our otherwise ordinary http server. (Remind me to draw a picture of how this is supposed work!)

For this demo:

> I have pointed `groggy.sleepy.muzoo.io` to a server 

We will set up an `https` proxy on this URL using sidecar containers from [`nginx-proxy`](https://github.com/nginx-proxy/nginx-proxy) and [`jrcs/letsencrypt-nginx-proxy-companion`](https://github.com/nginx-proxy/acme-companion). Long story short, we can set up these two "sidecar" containers as follows:

```bash
## start nginx-proxy
docker run --detach \
    --name nginx-proxy \
    --restart=always \
    --publish 80:80 \
    --publish 443:443 \
    --volume /etc/nginx/certs \
    --volume /etc/nginx/vhost.d \
    --volume /usr/share/nginx/html \
    --volume /var/run/docker.sock:/tmp/docker.sock:ro \
    jwilder/nginx-proxy:alpine

## start letsencrypt companion
 docker run --detach \
    --name nginx-proxy-letsencrypt \
    --restart=always \
    --volumes-from nginx-proxy \
    --volume /var/run/docker.sock:/var/run/docker.sock:ro \
    --env "DEFAULT_EMAIL={Your Email Here}" \ # <-- replace with your email here
    jrcs/letsencrypt-nginx-proxy-companion
```

Then... we can try booting up a normal `nginx` container to serve `https://groggy.sleepy.muzoo.io`, like so:

```bash
docker run --name groggy.sleepy-demo \
        --restart=always \
        --detach \
        --env "VIRTUAL_HOST=groggy.sleepy.muzoo.io" \
        --env "LETSENCRYPT_HOST=groggy.sleepy.muzoo.io" \
        nginx:latest
```

And voilà! access to ` `https://groggy.sleepy.muzoo.io is now possible and is `https`-enabled.

