#!/bin/bash

SCRIPT_ROOT="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
VERSION="1.21"

docker run --detach \
    --restart=always \
    --publish 80:80 \
    --name mynginx \
    --volume ${SCRIPT_ROOT}/www:/usr/share/nginx/html:ro \
    nginx:${VERSION}
