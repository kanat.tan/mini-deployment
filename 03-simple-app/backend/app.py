from flask import Flask, jsonify
import random

app = Flask(__name__)

@app.route('/random')
def lucky_draw():
    random_number = random.random()
    return jsonify({'source': 'fresh', 'random_number': random_number})
