#!/bin/bash

SCRIPT_ROOT="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

docker run --name myfrontend \
        --restart=always \
        --detach \
        myfrontend
