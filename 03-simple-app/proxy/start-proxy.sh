#!/bin/bash

SCRIPT_ROOT="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
VERSION="1.21"

docker run --name myproxy \
        --publish 80:80 \
        --restart=always \
        --link mybackend \
        --link myfrontend \
        --detach \
        --volume ${SCRIPT_ROOT}/default.conf:/etc/nginx/conf.d/default.conf \
        nginx:${VERSION}
